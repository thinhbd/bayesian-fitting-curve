


//Todo : minimize error function , and minimize error function with regularization
Array.matrix = function( m, n , initial )
{
    var a , i , j , mat = [];
    for( i = 0 ; i < m ; i++ )
    {
        a = [];
        for ( j = 0 ; j < n ; j++ )
        {
            a[j] = initial;
        }
        mat.push(a);
    }
    return mat;
}

//create identity matrix : matrix have diagional equal to 1 , other element is 0;
Array.identity = function( n )
{
    var mat = Array.matrix( n , n , 0 ) , id;
    for( id = 0 ; id < n ; id++ )
        mat[id][id] = 1;
    return mat;
}

//compute and return the multiplication of 2 matrix a , b;
var multiplyMatrix = function( a , b )
{
    var m = a.length;
    var n = a[0].length;
    var k = b[0].length;

    //var ret = Array.matrix(m , k , 0);
    //console.log(aa);

    var res = Array.matrix( m , k , 0 );
    //console.log(res);
    //document.writeln(ret.length , ret[0].length);
    for(var row = 0 ; row < m ; row++ )
    {
        for(var col = 0 ; col < k ; col++ )
            for(var id = 0 ; id < n ; id++ )
                res[row][col] += a[row][id] * b[id][col];
    }
    //console.log(ret);
    return res;
}

// compute transpose matrix of matrix A
var transposeMatrix = function( A )
{
    var row = A.length;
    var col = A[0].length;
    var ret = Array.matrix( col , row , 0 );
    //console.log(ret)
    var rowIndex , colIndex;
    for( rowIndex = 0 ; rowIndex < row ; rowIndex++ )
        for( colIndex = 0 ; colIndex < col ; colIndex++ )
            ret[colIndex][rowIndex] = A[rowIndex][colIndex];
    return ret;
}


var addMatrix = function( A , B )
{
    var row = A.length;
    var col = A[0].length;
    var ret = [];
    var tmp; // save sum for current row
    var rowIndex , colIndex;
    for( rowIndex = 0 ; rowIndex < row ; rowIndex++ )
    {
        tmp = [];
        for( colIndex = 0 ; colIndex < col ; colIndex++ )
            tmp[colIndex] = A[rowIndex][colIndex] + B[rowIndex][colIndex];
        ret[rowIndex] = tmp;
    }
    return ret;
}


// Generate Data Set for function f(x) = sin(2pi * x) with n point and adding noise.

//
var genDataSet = function(n)
{
    var point = [];
    var tmp , noise;
    for( var i = 0 ; i < n ; i++ )
    {
        tmp = Math.random();
        if( Math.random() < 0.5 ) noise = Math.random() * 0.1;
        else noise = -Math.random() * 0.1;
        point[i] = { x : tmp , y : Math.sin(2 * Math.PI * tmp) + noise };
    }
    //document.writeln(point.length);
   // console.log(point.length);
    return point;
}

//Fuction calculate f(x) for given polynomial coefficient
var f = function( val , coefficient )
{
    var ret = 0;
    var tmp = 1;
    for( var i = 0 ; i < coefficient.length ; i++ )
    {
        ret += tmp * coefficient[i];
        tmp *= val;
    }
    return ret;
}

//Function calculate vector {x^0 , x^1 , x^2 , ... , x^M }
var phi = function( x , degree )
{
    var ret = Array.matrix( degree + 1 , 1 , 0 );
    var i;
    //document.writeln(ret[0][0] , "adf");
    ret[0][0] = 1;
    for( i = 1 ; i <= degree ; i++ )
    {
        //document.writeln( ret[i - 1][0] ) ;
        ret[i][0] = ret[i - 1][0] * x;
    }
    return ret;
}

//Function swap two row(i , j) of matrix
var swapRow = function( A , i , j )
{
    var dim = A.length;
    for( var col = 0 ; col < dim ; col++ )
    {
        var tmp = A[i][col];
        A[i][col] = A[j][col];
        A[j][col] = tmp;
    }
}

//Row addition R1 = R1 + scalar * R2
var rowAdd = function( A  , Row2 , Row1 , scalar )
{
    var dim = A[0].length;
    for( var col = 0 ; col < dim ; col++ )
    {
        A[Row1][col] += A[Row2][col] * scalar;
    }
}

//Row scalar operation : row = row * scalar
var rowScalar = function( A , row , scalar )
{
    var dim = A[0].length;
    for( var col = 0 ; col < dim ; col++ )
        A[row][col] = A[row][col] *  scalar;
}


//Function calculate inverse matrix using Gauss Jordan algorithm
var inverseMatrix = function( A )
{
    var dim = A.length;
    var inverse = Array.identity(dim);
    var identityMatrix = Array.matrix(dim , dim , 0);
    var row , col // row index and column index
    //copy matrix

    for( row = 0 ; row < dim ; row++ )
        for( col = 0 ; col < dim ; col++ )
            identityMatrix[row][col] = A[row][col];

    var maxEle , id , tmp;
    for( col = 0 ; col < dim ; col++ )
    {
        maxEle = Math.abs(identityMatrix[col][col]);
        id = col;
        for( row = col + 1 ; row < dim ; row++ )
            if( Math.abs(identityMatrix[row][col]) > maxEle )
            {
                maxEle = Math.abs(identityMatrix[row][col]);
                id = row;
                //break;
            }

        //document.writeln(col + " " + id);
        //swap current row with id
        swapRow( identityMatrix , col , id );
        swapRow( inverse , col , id);

        //using row operation to set pivot element in current row ( equal to col )
        //document.writeln(identityMatrix);
        tmp = 1 / identityMatrix[col][col];
        rowScalar( identityMatrix , col , tmp);
        rowScalar( inverse , col , tmp);

        for( row = 0 ; row < dim ; row++ )
        {
            if( row === col ) continue;
            tmp = -identityMatrix[row][col];
            rowAdd( inverse , col , row , tmp );
            rowAdd( identityMatrix , col , row , tmp );
        }

    }

    return inverse;
    //scalar to identity matrix
    /*for( row = 0 ; row < dim ; row++ )
    {
        rowScalar( inverse , row ,  1 / identityMatrix[row][row] );
    }*/
}

/*var testInverseMatrix = [ [7 , 2 , 1] , [0 , 3 , -1 ] , [-3 , 4 , -2] ];
var inverseMatrixOutput = inverseMatrix(testInverseMatrix);

//document.writeln(inverseMatrixOutput);
//console.log(inverseMatrixOutput);
console.log( multiplyMatrix(testInverseMatrix , inverseMatrixOutput ) );*/


//matrix scalar operator
var matrixScalar = function( A , scalar )
{
    var row = A.length;
    var col = A[0].length;
    for( var i = 0 ; i < row ; i++ )
        for( var j = 0 ; j < col ; j++ )
            A[i][j] *= scalar;
}


/*var testMatrixScalar = [[1 , 2 , 3] , [1 , 0 , 1] , [3 , 2 , 1]];
matrixScalar(testMatrixScalar , 2);
console.log(testMatrixScalar);*/

/*calculate mean of predictive distribution given (DataSet , S , x , alpha , beta , degree )
    mean(x) = beta * transpose(phi(x)) * S * sum(phi(x[i] , degree)) * t[i]
    (x[i] , t[i]) is DataSet

    inverse(S) = alpha * I + beta * sum( phi(x[i] , degree) ) * transpose(phi(x));
*/
var meanFunction = function( DataSet , S , x , alpha , beta , degree )
{
    var mean = multiplyMatrix( transposeMatrix( phi(x , degree ) )  , S );
    var tmp = Array.matrix(degree + 1 , 1 , 0);
    var phiTmp;
    for( var i = 0 ; i < DataSet.length ; i++ )
    {
        phiTmp = phi(DataSet[i].x , degree);
        matrixScalar(phiTmp , DataSet[i].y);
        //document.writeln(tmp);
        tmp = addMatrix( phiTmp , tmp);
    }
    //mean = multiplyMatrix( mean , tmp );
//    document.writeln(mean);
//    return mean[0][0] * beta;
    return multiplyMatrix(mean , tmp)[0][0] * beta;
}

// Calculate matrix S for calculate mean and variance
var sCal = function( DataSet , alpha , beta , degree )
{
    
    var identityScalar = Array.identity( degree + 1 , degree + 1 ); 
    matrixScalar( identityScalar , alpha );// alpha * I

    var S;
    var invS;
    var phiX = phi( DataSet[0].x , degree ); // sum(phi(x) * transpose(phi(x)))
    phiX = multiplyMatrix( phiX , transposeMatrix(phiX) );
    
    var tmpPhi;
    for( var i = 1 ; i < DataSet.length ; i++ )
    {
        tmpPhi = phi(DataSet[i].x , degree);
        phiX = addMatrix( phiX , multiplyMatrix(tmpPhi , transposeMatrix(tmpPhi)) );
    }
    matrixScalar( phiX , beta );
    invS = addMatrix(identityScalar , phiX);
    S = inverseMatrix(invS);
    return S;
}

//mean curve generate
var meanCurvePoint = function( DataSet , alpha , beta , degree )
{
    var n = 100;
    var tmp = 0;
    var points = [];
    var S = sCal(DataSet , alpha , beta , degree);
    for( var i = 0 ; i < n ; i++ )
    {
        //invS = addMatrix(identityScalar , multiplyMatrix(phiX , transposeMatrix(phi(tmp , degree) ) ) );
        //S = inverseMatrix(invS);
        points[i] = { x : tmp , y : meanFunction( DataSet , S , tmp , alpha , beta , degree) };
        tmp += 1/ n;
    }
    return points;
}

//variance calculate for point at x
var varianceFunction = function( DataSet , S , x , alpha , beta , degree )
{
    var phiX = phi(x , degree);
    var variance = multiplyMatrix(transposeMatrix(phiX) , S);
    variance = multiplyMatrix( variance , phiX);
    return variance[0][0] + 1 / beta;
}

//generae +/- standard devitaion around mean points
var aroundMeanPoints = function( DataSet , alpha , beta , degree , meanData , sign)
{
    var tmp = 0;
    var points = [];
    var S = sCal(DataSet , alpha , beta , degree);
    for( var i = 0 ; i < meanData.length ; i++ )
    {
        tmp = varianceFunction( DataSet , S , meanData[i].x , alpha , beta , degree);
        tmp = Math.sqrt(tmp);
        points[i] = { x : meanData[i].x , y : tmp * sign + meanData[i].y };
    }
    return points;
}
//
//
//Generate point for drawing graph
var genPointForGraph = function ( coefficient )
{
    var type = 1;
    if( coefficient === -1 )
        type = 0; // calculate polynomial function;
   // document.writeln(coefficient);
    //document.writeln(type);
    var points = [];
    var n = 100;
    var tmp = 0;
    for( var i = 0 ; i < n ; i++ )
    {
        if( type === 0 )
            points[i] = { x : tmp , y : Math.sin(2 * Math.PI * tmp) };
        else
            points[i] = { x : tmp , y : f(tmp , coefficient) };

        tmp += 1/n;
    }
    return points;
}



var margin = {top: 30, right: 30, bottom: 30, left: 30},
    width = 660 - margin.left - margin.right,
    height = 360 - margin.top - margin.bottom;


// Set the ranges
var x = d3.scale.linear().range([0, width]);
var y = d3.scale.linear().range([height, 0]);

// Define the axes
var xAxis = d3.svg.axis().scale(x)
    .orient("bottom").ticks(5);

var yAxis = d3.svg.axis().scale(y)
    .orient("left").ticks(5);

// Define the line
var valueline = d3.svg.line()
    .x(function(d) { return x(d.x); })
    .y(function(d) { return y(d.y); });


var svg = d3.select("body")
    .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
    .append("g")
        .attr("transform",
              "translate(" + margin.left + "," + margin.top + ")");


//draw function from given points

var DataSet = genDataSet(10);
var originalPoints = genPointForGraph(-1); //point to draw sin(2pi * x);


//var coef = curveFittingSolve( DataSet , 3 , 0 );
//document.writeln(coef);
//var curveFittingPoint = genPointForGraph(coef);

var alpha = 5 * 0.001;
var beta = 11.1;
var degree = 9;
var bayesianFittingCurvePoint = meanCurvePoint( DataSet , alpha , beta , degree );
var upperMeanPoints = aroundMeanPoints(DataSet , alpha , beta , degree , bayesianFittingCurvePoint , 1);
var lowerMeanPoints = aroundMeanPoints(DataSet , alpha , beta , degree , bayesianFittingCurvePoint , -1);
/*for( let i = 0 ; i < bayesianFittingCurvePoint.length ; i++ )
{
    document.writeln( "(" + bayesianFittingCurvePoint[i].x + "," + bayesianFittingCurvePoint[i].y  + ")" );
}*/

// Adds the svg canvas

// Define the fitting line
var fittingline = d3.svg.line()
    .x(function(d) { return x(d.x); })
    .y(function(d) { return y(d.y); });

var svg = d3.select("body")
    .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
    .append("g")
        .attr("transform",
              "translate(" + margin.left + "," + margin.top + ")");


    // Scale the range of the data
    x.domain(d3.extent(originalPoints, function(d) { return d.x; }));
    y.domain(d3.extent(originalPoints, function(d) { return d.y; }));

    // Add the valueline path.
    svg.append("path")
        .attr("class", "line")
        .attr("d", valueline(originalPoints))
        .attr("stroke" , "green");

    svg.append("path")
        .attr("class", "line")
        .attr("d", valueline(bayesianFittingCurvePoint))
        .attr("stroke" , "red");

    svg.append("path")
        .attr("class", "line")
        .attr("d", valueline(upperMeanPoints))
        .attr("stroke" , "blue");
    
    
    svg.append("path")
        .attr("class", "line")
        .attr("d", valueline(lowerMeanPoints))
        .attr("stroke" , "blue");
    // Add the X Axis
    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    // Add the Y Axis
    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis);

var circle = svg.selectAll("circle").data(DataSet);

circle.enter().append("circle")
	.attr("class", "circles")
	.attr({
		cx: function (d) {
            return x(d.x);
        },
        cy: function (d) {
            return y(d.y);
        },
        r: 4
    })
    .style("fill", "black");


//draw polynomial fitting curve
