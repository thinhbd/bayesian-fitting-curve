


//Todo : minimize error function , and minimize error function with regularization
Array.matrix = function( m, n , initial )
{
    var a , i , j , mat = [];
    for( i = 0 ; i < m ; i++ )
    {
        a = [];
        for ( j = 0 ; j < n ; j++ )
        {
            a[j] = initial;
        }
        mat.push(a);
    }
    return mat;
}

//create identity matrix : matrix have diagional equal to 1 , other element is 0;
Array.identity = function( n )
{
    var mat = Array.matrix( n , n , 0 ) , id;
    for( id = 0 ; id < n ; id++ )
        mat[id][id] = 1;
    return mat;
}

//compute and return the multiplication of 2 matrix a , b;
var multiplyMatrix = function( a , b )
{
    var m = a.length;
    var n = a[0].length;
    var k = b[0].length;

    //var ret = Array.matrix(m , k , 0);
    //console.log(aa);

    var res = Array.matrix( m , k , 0 );
    console.log(res);
    //document.writeln(ret.length , ret[0].length);
    for(var row = 0 ; row < m ; row++ )
    {
        for(var col = 0 ; col < k ; col++ )
            for(var id = 0 ; id < n ; id++ )
                res[row][col] += a[row][id] * b[id][col];
    }
    //console.log(ret);
    return res;
}

// compute transpose matrix of matrix A
var transposeMatrix = function( A )
{
    var row = A.length;
    var col = A[0].length;
    var ret = Array.matrix( col , row , 0 );
    //console.log(ret)
    var rowIndex , colIndex;
    for( rowIndex = 0 ; rowIndex < row ; rowIndex++ )
        for( colIndex = 0 ; colIndex < col ; colIndex++ )
            ret[colIndex][rowIndex] = A[rowIndex][colIndex];
    return ret;
}


var addMatrix = function( A , B )
{
    var row = A.length;
    var col = A[0].length;
    var ret = [];
    var tmp; // save sum for current row
    var rowIndex , colIndex;
    for( rowIndex = 0 ; rowIndex < row ; rowIndex++ )
    {
        tmp = [];
        for( colIndex = 0 ; colIndex < col ; colIndex++ )
            tmp[colIndex] = A[rowIndex][colIndex] + B[rowIndex][colIndex];
        ret[rowIndex] = tmp;
    }
    return ret;
}

/*var testMatrixA = [
    [ 1 , 2],
    [ 4 , 5],
    [ -1 , -2],
];

console.log( multiplyMatrix(transposeMatrix(testMatrixA) , testMatrixA ) );
var testMatrixB = [
    [ 1 , 2 , 3 , 5 ],
    [ 4 , 5 , 6 , -7],
    [ -1 , -2 , -3 , 0],
];

var testMatrixC = [
    [-1 , 3],
    [2 , 5],
];


/*  Gauss Elimination to find the solution for equation Ax = b with parameter is augenmented matrix ( A | b )
    Test matrix
    8 -3
*/


var gaussElimination = function( A )
{

    /*var n = A.length;
    var rowIndex , currentColumn , currentRow , colIndex;
    var maxRowId;
    var tmp , c;

    for( currentColumn = 0 ; currentColumn < n ; currentColumn++ )
    {
        //find row with max A[][currentColumn]
        maxRowId = currentColumn;
        for( rowIndex = currentColumn ; rowIndex < n ; rowIndex++ )
            if( A[maxRowId][currentColumn] < A[rowIndex][currentColumn] )
                maxRowId = rowIndex;

        //swap max row with the currentRow ( currentRow = currentColumn )
        currentRow = currentColumn;
        for( colIndex = 0 ; colIndex <= n ; colIndex++ )
        {
            tmp = A[currentRow][colIndex];
            A[currentRow][colIndex] = A[maxRowId][colIndex];
            A[maxRowId][colIndex] = tmp;
        }

        //make column below currentRow equal to zero
        for( rowIndex = currentRow + 1 ; rowIndex < n ; rowIndex++ )
        {
            if( A[rowIndex][currentColumn] === 0.0 ) continue;
            c = -1 * A[currentRow][currentColumn] / A[rowIndex][currentColumn];
            for( colIndex = currentColumn ; colIndex <= n ; colIndex++ )
                A[rowIndex][colIndex] = A[currentRow][colIndex] + A[rowIndex][colIndex] * c;
        }
    }
    //document.writeln(A);
    //backsubtitusion to get the result
    var ret = [];
    for( rowIndex = 0 ; rowIndex < n ; rowIndex++ )
            ret[rowIndex] = 0;
    for( rowIndex = n - 1;  rowIndex >= 0 ; rowIndex-- )
    {
        ret[rowIndex] = A[rowIndex][n];
        for( colIndex = n - 1 ; colIndex > rowIndex ; colIndex-- )
            ret[rowIndex] -= A[rowIndex][colIndex] * ret[colIndex][0];
        ret[rowIndex] /= A[rowIndex][rowIndex];
    }
    return ret;*/   var n = A.length;

    for (var i=0; i<n; i++) {
        // Search for maximum in this column
        var maxEl = Math.abs(A[i][i]);
        var maxRow = i;
        for(var k=i+1; k<n; k++) {
            if (Math.abs(A[k][i]) > maxEl) {
                maxEl = Math.abs(A[k][i]);
                maxRow = k;
            }
        }

        // Swap maximum row with current row (column by column)
        for (var k=i; k<n+1; k++) {
            var tmp = A[maxRow][k];
            A[maxRow][k] = A[i][k];
            A[i][k] = tmp;
        }

        // Make all rows below this one 0 in current column
        for (k=i+1; k<n; k++) {
            var c = -A[k][i]/A[i][i];
            for(var j=i; j<n+1; j++) {
                if (i==j) {
                    A[k][j] = 0;
                } else {
                    A[k][j] += c * A[i][j];
                }
            }
        }
    }

    // Solve equation Ax=b for an upper triangular matrix A
    var x= new Array(n);
    for (var i=n-1; i>-1; i--) {
        x[i] = A[i][n]/A[i][i];
        for (var k=i-1; k>-1; k--) {
            A[k][n] -= A[k][i] * x[i];
        }
    }
    return x;

}

/*var testGaussA = [
    [1 , 5 , 7],
    [-2, -7 , -5],
]; // ans = { 8 , -3}

var testGaussB = [
    [0 , 2 , 1 , -8],
    [1 , -2 , -3 , 0],
    [-1 , 1 , 2 , 3],
];*/


//document.writeln( gaussElimination(testGaussA) );
//document.writeln( gaussElimination(testGaussB) );

/*  Solve the curve fitting problem with given dataset and polynomial degree
    Dataset , point array with object(x , y) coordinate
    lambda use for regularization
    Equation:

    {expMatrix * transpose(expMatrix) + lambdaIdentity} * w = expMatrix * targetMatrix;
    <----------------------A--------------------------> * w = <------------B---------->

    expMatrix = [
        1       1       1    ...  1
        x[0]   x[1]   x[2]   ... x[N - 1]
        x[0]^2 x[1]^2 x[2]^2 ... x[N - 1]^2
        .
        .
        .
        x[0]^M x[1]^M x[2]^M ... x[N - 1] ^ M;

    ] (M + 1 ) * N matrix

    lambdaIdentity =[
        lambda  0       0    ...     0
        0       lambda  0    ...     0
        .       .       .            .
        .       .       .            .
        0       0       0    ...     lambda

    ]  (M + 1) * (M + 1) matrix

*/



/*var curveFittingSolve = function( dataset , polynomialDegree , lambda )
{
    var M = polynomialDegree;
    var N = dataset.length;
    //var rowIndex , colIndex;
    //create exponential matrix
    var expMatrix = Array.matrix( M + 1 , N  , 0 );

    for( var colIndex = 0 ; colIndex < N ; colIndex++ )
        expMatrix[0][colIndex] = 1;

    for( var rowIndex = 1 ; rowIndex <= M ; rowIndex++ )
        for( var colIndex = 0 ; colIndex < N ; colIndex++ )
            expMatrix[rowIndex][colIndex] = expMatrix[rowIndex - 1][colIndex] * dataset[colIndex].x;

    var agumentedMatrix = multiplyMatrix(expMatrix , transposeMatrix(expMatrix));

    var targetMatrix = Array.matrix( N , 1 , 0 );
    for( colIndex = 0 ; colIndex < N ; colIndex++ )
        targetMatrix[colIndex][0]  = dataset[colIndex].y;

    //create lambda identity matrix

    /*var lambdaMatrix = Array.matrix( M + 1 , M + 1 , 0 );
    for( var i = 0 ; i < M + 1 ; i++ )
        lambdaMatrix[i][i] = lambda;

    console.log(lambdaMatrix);
    /*
        A = A + lambdaMatrix;
    */
    //console.log(addMatrix( A , lambdaMatrix ))

    //console.log(A);

    /*
    ** B = expMatrix * targetMatrix;
    */

    //var valMatrix = multiplyMatrix( expMatrix , targetMatrix );
    //create augenmented matrix A by concat B to it
    //console.log(agumentedMatrix);
    //for( rowIndex = 0 ; rowIndex <= M ; rowIndex++ )
    //    agumentedMatrix[rowIndex].push( valMatrix[rowIndex][0] );

    //console.log(agumentedMatrix);
    //return gaussElimination(agumentedMatrix);
//}

// Generate Data Set for function f(x) = sin(2pi * x) with n point and adding noise.

//
var genDataSet = function(n)
{
    var point = [];
    var tmp , noise;
    for( var i = 0 ; i < n ; i++ )
    {
        tmp = Math.random();
        if( Math.random() < 0.5 ) noise = Math.random() * 0.1;
        else noise = -Math.random() * 0.1;
        point[i] = { x : tmp , y : Math.sin(2 * Math.PI * tmp) + noise };
    }
    //document.writeln(point.length);
   // console.log(point.length);
    return point;
}

//Fuction calculate f(x) for given polynomial coefficient
var f = function( val , coefficient )
{
    var ret = 0;
    var tmp = 1;
    for( var i = 0 ; i < coefficient.length ; i++ )
    {
        ret += tmp * coefficient[i];
        tmp *= val;
    }
    return ret;
}
//Generate point for drawing graph
var genPointForGraph = function ( coefficient )
{
    var type = 1;
    if( coefficient === -1 )
        type = 0; // calculate polynomial function;
   // document.writeln(coefficient);
    document.writeln(type);
    var points = [];
    var n = 100;
    var tmp = 0;
    for( var i = 0 ; i < n ; i++ )
    {
        if( type === 0 )
            points[i] = { x : tmp , y : Math.sin(2 * Math.PI * tmp) };
        else
            points[i] = { x : tmp , y : f(tmp , coefficient) };

        tmp += 1/n;
    }
    return points;
}




//document.writeln( transposeMatrix ( testMatrixA ) );
//document.writeln( addMatrix( testMatrixA , testMatrixB ) );
//document.writeln( multiplyMatrix( testMatrixA , testMatrixC ) );
//var gaussElimination( )


var margin = {top: 30, right: 30, bottom: 30, left: 30},
    width = 660 - margin.left - margin.right,
    height = 360 - margin.top - margin.bottom;


// Set the ranges
var x = d3.scale.linear().range([0, width]);
var y = d3.scale.linear().range([height, 0]);

// Define the axes
var xAxis = d3.svg.axis().scale(x)
    .orient("bottom").ticks(5);

var yAxis = d3.svg.axis().scale(y)
    .orient("left").ticks(5);

// Define the line
var valueline = d3.svg.line()
    .x(function(d) { return x(d.x); })
    .y(function(d) { return y(d.y); });


var svg = d3.select("body")
    .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
    .append("g")
        .attr("transform",
              "translate(" + margin.left + "," + margin.top + ")");


//draw function from given points

var DataSet = genDataSet(10);
var originalPoints = genPointForGraph(-1); //point to draw sin(2pi * x);


var coef = curveFittingSolve( DataSet , 3 , 0 );
//document.writeln(coef);
var curveFittingPoint = genPointForGraph(coef);
/*for( var i = 0 ; i < curveFittingPoint.length ; i++ )
{
    document.writeln( "(" + curveFittingPoint[i].x + "," + curveFittingPoint[i].y + ")" );
}*/

// Adds the svg canvas

// Define the line
var fittingline = d3.svg.line()
    .x(function(d) { return x(d.x); })
    .y(function(d) { return y(d.y); });

var svg = d3.select("body")
    .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
    .append("g")
        .attr("transform",
              "translate(" + margin.left + "," + margin.top + ")");


    // Scale the range of the data
    x.domain(d3.extent(originalPoints, function(d) { return d.x; }));
    y.domain(d3.extent(originalPoints, function(d) { return d.y; }));

    // Add the valueline path.
    svg.append("path")
        .attr("class", "line")
        .attr("d", valueline(originalPoints))
        .attr("stroke" , "green");

    svg.append("path")
        .attr("class", "line")
        .attr("d", fittingline(curveFittingPoint))
        .attr("stroke" , "red");


    // Add the X Axis
    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    // Add the Y Axis
    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis);

var circle = svg.selectAll("circle").data(DataSet);

circle.enter().append("circle")
	.attr("class", "circles")
	.attr({
		cx: function (d) {
            return x(d.x);
        },
        cy: function (d) {
            return y(d.y);
        },
        r: 4
    })
    .style("fill", "blue");


//draw polynomial fitting curve
